# Template
This is a template for any subrepositories for the re:human project and any of
its subprojects. It includes a license, by default the MIT license, contributing
guidelines, and a basic format for layout of source code.