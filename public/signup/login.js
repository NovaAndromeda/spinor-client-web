function ready() {
  var loginbtn = document.querySelector("#login");
  var username = document.querySelector("#username");
  var password = document.querySelector("#password");

  var username_min = "1";
  var username_max = "24";
  var password_min = "12";
  var password_max = "64";

  var counter_u = document.querySelector("#username_c");
  var counter_p = document.querySelector("#password_c");

  var err = document.querySelector("#err");
  var err_showing = false;


  // fetch("/api/rsa/login")
  //   .then(response => response.json())
  //   .then(data => window.serverPublic = data);
  // fetch("/api/isloggedin")
  //   .then(response => response.text())
  //   .then(data => (function(){
  //     if (data=='True') {
  //       location.replace("/chat");
  //     }
  //   })(data));

  function login() {
    data = {
      username: username.value,
      password: password.value
    };
    
    // try {
    //   $.post("/api/signup", {data: $crypt.encode(serverPublic, JSON.stringify(data))}, get_signup_result);
    // } catch {
    //   location.reload();
    // }
  }
  
  loginbtn.addEventListener("click", login); 

  function get_signup_result(result) {
    if(result=="True"){
      window.location.replace("/chat");
    } else {
      err.style.display = "flex";
      err_showing = true;
    }
  }

  function username_keydown(e={key:""}){
    username.minlength = username_min;
    username.maxlength = username_max;
    username_c.innerText = username.value.length+"/"+username_max;
    if(username.value.length<1){username_c.style.color = "rgb(255,0,0)";} else {username_c.style.color="rgb(0,255,0)";}
    if (e.key == "Enter") {
      password.select();
    }
    if (err_showing) {
      err.style.display = "none";
      err_showing = false;
    }
  }
  username.addEventListener("keyup", username_keydown);

  function password_keydown(e={key:""}){
    password.minlength = password_min;
    password.maxlength = password_max;
    password_c.innerText = password.value.length+"/"+password_max;
    if(password.value.length<8){password_c.style.color = "rgb(255,0,0)";} else if(password.value.length<16){password_c.style.color = "rgb(255,127,0)";} else {password_c.style.color = "rgb(0,255,0)";}
    if (e.key == "Enter") {
      login();
    }
    if (err_showing) {
      err.style.display = "none";
      err_showing = false;
    }
  }
  password.addEventListener("keyup", password_keydown);

  username_keydown();
  password_keydown();
}